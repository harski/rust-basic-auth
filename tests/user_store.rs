use basic_auth::UserStore;
use std::collections::HashMap;

pub struct TestUserStore {
    users: HashMap<String, String>,
}

impl Default for TestUserStore {
    fn default() -> Self {
        let mut users = HashMap::new();
        users.insert("user".to_string(), "password".to_string());

        TestUserStore { users }
    }
}

impl UserStore for TestUserStore {
    fn check_password(&self, user: &str, password: &str) -> Option<bool> {
        self.users.get(user).map(|pw| password.eq(pw))
    }
}
