extern crate basic_auth;
mod user_store;
use basic_auth::authenticate;
use basic_auth::Error;
use user_store::TestUserStore;

#[test]
fn test_check_header_invalid_password() {
    let user_store = TestUserStore::default();
    // "user:pass"
    let header = "Authorization: Basic dXNlcjpwYXNz";
    let res = authenticate(header, &user_store);
    assert_eq!(Err(Error::InvalidPassword), res);
}

#[test]
fn test_check_header_invalid_user() {
    let user_store = TestUserStore::default();
    // "user2:password"
    let header = "Authorization: Basic dXNlcjI6cGFzc3dvcmQ=";
    let res = authenticate(header, &user_store);
    assert_eq!(Err(Error::UserNotFound), res);
}

#[test]
fn test_check_header_valid_user() {
    let user_store = TestUserStore::default();
    // "user:password"
    let header = "Authorization: Basic dXNlcjpwYXNzd29yZA==";
    assert_eq!("user", authenticate(header, &user_store).unwrap());
}

#[test]
fn test_missing_user() {
    let user_store = TestUserStore::default();
    // ":password"
    let header = "Authorization: Basic OnBhc3N3b3Jk";
    assert_eq!(
        Err(Error::InvalidCredentialString),
        authenticate(header, &user_store)
    );
}
