use crate::error::{Error, Result};
use crate::user_store::UserStore;
use std::str::from_utf8;

const HEADER_STR: &str = "Authorization: Basic ";
const HEADER_LEN: usize = HEADER_STR.len();

struct Credentials {
    pub username: String,
    pub password: String,
}

/// Authenticate a user identified by a HTTP Basic Auth header against a UserStore backend.
///
/// ```compile_fail
/// let header = "Authorization: Basic dXNlcjpwYXNzd29yZA==";
/// let user_store = TestUserStore::new();
/// if authenticate(header, &user_store)? {
///     // ...
/// }
/// ```
pub fn authenticate<T: UserStore>(header: &str, user_store: &T) -> Result<String> {
    let credentials = get_credentials_from_header(header)?;
    match user_store.check_password(&credentials.username, &credentials.password) {
        Some(true) => Ok(credentials.username),
        Some(false) => Err(Error::InvalidPassword),
        None => Err(Error::UserNotFound),
    }
}

/// Get auth credentials from base64 encoded &str.
fn decode_credentials(base64: &str) -> Result<Credentials> {
    let decoded = base64::decode(base64)?;
    let decoded_as_utf8 = from_utf8(&decoded)?;
    let split_v: Vec<&str> = decoded_as_utf8.splitn(2, ':').collect();

    if split_v.len() != 2 || split_v[0].is_empty() {
        Err(Error::InvalidCredentialString)
    } else {
        Ok(Credentials {
            username: split_v[0].to_string(),
            password: split_v[1].to_string(),
        })
    }
}

/// Get auth credentials from HTTP Basic Auth header.
fn get_credentials_from_header(header: &str) -> Result<Credentials> {
    if header.len() > HEADER_LEN && header.starts_with(HEADER_STR) {
        decode_credentials(&header[HEADER_LEN..])
    } else {
        Err(Error::InvalidCredentialString)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_credential_decode() {
        let base64 = "dXNlcjpwYXNzd29yZA==";
        let credentials = decode_credentials(base64).unwrap();
        assert_eq!("user", credentials.username);
        assert_eq!("password", credentials.password);
    }

    #[test]
    fn test_valid_header_handling() {
        let header = "Authorization: Basic dXNlcjpwYXNzd29yZA==";
        let credentials = get_credentials_from_header(header).unwrap();
        assert_eq!("user", credentials.username);
        assert_eq!("password", credentials.password);
    }
}
