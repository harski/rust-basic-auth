pub trait UserStore {
    fn check_password(&self, user: &str, password: &str) -> Option<bool>;
}
