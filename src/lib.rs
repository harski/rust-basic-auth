//! This crate is used for checking the HTTP Basic Auth header against a
//! [UserStore](trait.UserStore.html).
//!
//! To use the library, implement the `UserStore` trait to your backend and use the
//! [check_from_header](fn.check_from_header.html) function to authenticate users.

mod basic_auth;
mod error;
mod user_store;

pub use crate::basic_auth::authenticate;
pub use crate::error::Error;
pub use crate::error::Result;
pub use crate::user_store::UserStore;
