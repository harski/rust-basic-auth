use std::error;
use std::fmt;
use std::str::Utf8Error;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, PartialEq)]
pub enum Error {
    Base64Decode(base64::DecodeError),
    InvalidCredentialString,
    InvalidPassword,
    Utf8(Utf8Error),
    UserNotFound,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Base64Decode(ref e) => e.fmt(f),
            Error::InvalidCredentialString => write!(f, "Invalid credential string."),
            Error::InvalidPassword => write!(f, "Invalid password."),
            Error::Utf8(ref e) => e.fmt(f),
            Error::UserNotFound => write!(f, "User not found in the user store."),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            Error::Base64Decode(ref e) => Some(e),
            Error::Utf8(ref e) => Some(e),
            _ => None,
        }
    }
}

impl From<base64::DecodeError> for Error {
    fn from(err: base64::DecodeError) -> Error {
        Error::Base64Decode(err)
    }
}

impl From<Utf8Error> for Error {
    fn from(err: Utf8Error) -> Error {
        Error::Utf8(err)
    }
}
